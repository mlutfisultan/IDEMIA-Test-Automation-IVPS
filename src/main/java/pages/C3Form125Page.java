package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class C3Form125Page {
    WebDriver driver;

    public By Spiner = By.cssSelector("fa-solid fa-spinner label-color fa-spin loader-size"); ////app-form125-generator//i[@class='fa-solid fa-spinner label-color fa-spin loader-size']
    public By Page_Spiner = By.cssSelector("fa-solid fa-spinner label-color fa-spin loader-size");
    public By Form_125_Generator_Btn = By.id("pages-entry-form125-generator");

    public void Select_Form_125_Generator_Btn() {
        ElementActions.click(driver, Form_125_Generator_Btn);
    }

    public By Form_125_Radar_Code_Btn = By.id("violations-filter-parameters-radarcode-input");

    public void Select_Form_125_Radar_Code_Btn() {
        ElementActions.click(driver, Form_125_Radar_Code_Btn);
    }

    public By Form_125_Arrangement_Type = By.id("violations-filter-parameters-sort-type-input");// Drop Menu

    public void Select_Form_125_Arrangement_Type() {
        ElementActions.click(driver, Form_125_Arrangement_Type);
    }

    public By Form_125_Arrange_Type_ASC = By.id("violations-filter-parameters-sort-type-input-option-asc");

    public void Select_Form_125_Arrangement_Type_Asc() {
        ElementActions.click(driver, Form_125_Arrange_Type_ASC);
    }

    public By Form_125_Arrange_Type_DSC = By.id("violations-filter-parameters-sort-type-input-option-desc");

    public void Select_Form_125_Arrangement_Type_Dsc() {
        ElementActions.click(driver, Form_125_Arrange_Type_DSC);
    }

    public By Form_125_Violation_From_Date_Input = By.cssSelector("#violations-filter-parameters-violation-date-input .ng-tns-c85-5.p-inputtext.p-component.ng-star-inserted");

    public void Type_Form_125_Violation_From_Date() {

        ElementActions.type(driver, Form_125_Violation_From_Date_Input, "2020-01-01");
    }

    public By Form_125_Review_Activation = By.id("violations-filter-parameters-review-form-input");

    public void Select_Form_125_Review_Activation() {
        ElementActions.click(driver, Form_125_Review_Activation);
    }

    public By Form_125_Review_Activation_Yes = By.id("violations-filter-parameters-review-form-input-option-yes");

    public void Select_Form_125_Review_Activation_Yes() {
        ElementActions.click(driver, Form_125_Review_Activation_Yes);
    }

    public By Form_125_Review_Activation_No = By.id("violations-filter-parameters-review-form-input-option-no");

    public void Select_Form_125_Review_Activation_No() {
        ElementActions.click(driver, Form_125_Review_Activation_No);
    }

    public By Form_125_View_Violations_Btn = By.id("violations-filter-parameters-show-violations-button");

    public void Select_Form_125_View_Violations_Btn() {
        ElementActions.click(driver, Form_125_View_Violations_Btn);
    }

    public By Form_125_Cancel_Btn = By.id("violations-filter-parameters-cancel-button");

    public void Select_Form_125_Cancel_Btn() {
        ElementActions.click(driver, Form_125_Cancel_Btn);
    }

    // Form 125 Genertor Methods

    public By Form_125_old_Plate_ID_CBox = By.id("form125-generator-oldPlateNumber-input");

    public void Select_Form_125_old_Plate_ID_CBox() {
        ElementActions.click(driver, Form_125_old_Plate_ID_CBox);
    }

    public By Form_125_Cities_Menu = By.id("form125-generator-governorates-input");

    public void Select_Form_125_Cities_Menu() {
        ElementActions.click(driver, Form_125_Cities_Menu);
    }

    public By Form_125_old_Paintings_CitySearch = By.cssSelector("p-dropdownitem:nth-child(14) > .p-dropdown-item");

    public void Search_Form_125_old_Paintings_City() {
        ElementActions.click(driver, Form_125_old_Paintings_CitySearch);
    }
    public By Form_125_Plate_Source_Locator = By.cssSelector("div.move");
    public By Form_125_Plate_Destination_Locator = By.cssSelector("img.source-image.ng-star-inserted");

    public void Drag_Plate_Identifier()
    {
        ElementActions.dragAndDrop(driver,Form_125_Plate_Source_Locator,Form_125_Plate_Destination_Locator);
    }

    public By Form_125_Plate_ID_1 = By.cssSelector("#cpn1.plate-character");// Text Area

    public void Type_Form_125_Plate_ID_1() {
        ElementActions.keyPress(driver, Form_125_Plate_ID_1, "ع");
    }

    public By Form_125_Plate_ID_2 = By.cssSelector("#cpn2.plate-character");// Text Area

    public void Type_Form_125_Plate_ID_2() {
        ElementActions.type(driver, Form_125_Plate_ID_2, "ب");
    }

    public By Form_125_Plate_ID_3 = By.cssSelector("#cpn3.plate-character");// Text Area

    public void Type_Form_125_Plate_ID_3() {
        ElementActions.type(driver, Form_125_Plate_ID_3, "ت");
    }

    public By Form_125_Plate_ID_4 = By.cssSelector("#cpn4.plate-character");// Text Area

    public void Type_Form_125_Plate_ID_4() {
        ElementActions.type(driver, Form_125_Plate_ID_4, "");
    }

    public By Form_125_Plate_ID_5 = By.cssSelector("#cpn5.plate-numbers");// Text Area

    public void Type_Form_125_Plate_ID_5() {

        ElementActions.type(driver, Form_125_Plate_ID_5, "1234");
    }

    public void Type_Form_125_Plate_ID() {
        ElementActions.type(driver, Form_125_Plate_ID_5, "2020");
        ElementActions.type(driver, Form_125_Plate_ID_2, "ب");
        ElementActions.type(driver, Form_125_Plate_ID_1, "م");
        ElementActions.type(driver, Form_125_Plate_ID_3, "ت");
        //ElementActions.type(driver, Form_125_Plate_ID_4, "ت");

    }

    //public By Form_125_Type_of_Offense = By.id("form125-generator-select-violations-input");// Drop Menu
    public By Form_125_Type_of_Offense = By.cssSelector("div.ng-tns-c77-6.p-multiselect-trigger");// Drop Menu

    public void Select_Form_125_Type_of_Offense() {
        ElementActions.click(driver, Form_125_Type_of_Offense);
    }

    public By Form_125_Type_Offense_Child1 = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");

    public void Select_Form_125_Type_Offense_Child1() {
        ElementActions.click(driver, Form_125_Type_Offense_Child1);
    }

    public By Form_125_Type_Offense_Child2 = By.cssSelector("p-multiselectitem:nth-child(2)  > .p-multiselect-item");



    public void Select_Form_125_Type_Offense_Child2() {
        ElementActions.click(driver, Form_125_Type_Offense_Child2);
    }

    public By Form_125_Type_Offense_Child3 = By.cssSelector("p-multiselectitem:nth-child(3)  > .p-multiselect-item");

    public void Select_Form_125_Type_Offense_Child3() {
        ElementActions.click(driver, Form_125_Type_Offense_Child3);
    }


    public By Form_125_Type_Offense_Child5 = By.cssSelector("p-multiselectitem:nth-child(5)  > .p-multiselect-item");

    public void Select_Form_125_Type_Offense_Child5() {
        ElementActions.click(driver, Form_125_Type_Offense_Child5);
    }

    public By Form_125_Type_Offense_Child6 = By.cssSelector("p-multiselectitem:nth-child(6)  > .p-multiselect-item");

    public void Select_Form_125_Type_Offense_Child6() {
        ElementActions.click(driver, Form_125_Type_Offense_Child6);
    }

    public By Form_125_Type_Offense_Child7 = By.cssSelector("p-multiselectitem:nth-child(7)  > .p-multiselect-item");

    public void Select_Form_125_Type_Offense_Child7() {
        ElementActions.click(driver, Form_125_Type_Offense_Child7);
    }

    public By Form_125_Type_Offense_Child8 = By.cssSelector("p-multiselectitem:nth-child(8)  > .p-multiselect-item");

    public void Select_Form_125_Type_Offense_Child8() {
        ElementActions.click(driver, Form_125_Type_Offense_Child8);
    }

    public void Select_List_Of_Form_125_Type_Offense_Child() {
        ElementActions.click(driver, Form_125_Type_Offense_Child1);
        ElementActions.click(driver, Form_125_Type_Offense_Child2);
        ElementActions.click(driver, Form_125_Type_Offense_Child3);
        ElementActions.click(driver, Form_125_Type_Offense_Child5);
        /*ElementActions.click(driver, Form_125_Type_Offense_Child6);
        ElementActions.click(driver, Form_125_Type_Offense_Child7);
        ElementActions.click(driver, Form_125_Type_Offense_Child8);*/
    }

    public By Offenses_List_closer_Btn = By.cssSelector(".p-multiselect-close-icon");

    public void Select_Offenses_List_closer_Btn() {
        ElementActions.click(driver, Offenses_List_closer_Btn);
    }

    public By Form_125_Creation_Btn = By.id("form125-generator-submit-form125-button");

    public void Select_Form_125_Creation_Btn() {
        ElementActions.hoverAndClick(driver, Form_125_Creation_Btn, Form_125_Creation_Btn);
    }

    public By Form_125_Full_View_Image_Btn = By.id("form125-generator-view-full-image-button");

    public void Select_Form_125_Full_View_Image_Btn() {
        ElementActions.click(driver, Form_125_Full_View_Image_Btn);
    }

    public By Form_125_Skip_Violation_Btn = By.id("form125-generator-skip-violation-button");

    public void Select_Form_125_Skip_Offense_Btn() {
        ElementActions.click(driver, Form_125_Skip_Violation_Btn);
    }

    public By Form_125_Vehicle_Image_Processing_Btn = By.id("image-cropper-view-cropped-image-button");

    public void Select_Form_125_Vehicle_Image_Processing_Btn() {
        ElementActions.click(driver, Form_125_Vehicle_Image_Processing_Btn);
    }

    public C3Form125Page(WebDriver driver) {
        this.driver = driver;
    }
}
