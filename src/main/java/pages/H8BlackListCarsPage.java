package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class H8BlackListCarsPage {
    WebDriver driver;
    public By List_Prohibited_Cars_Btn = By.id("main-menu-f87eb223e99e4a442");

    public void Select_List_Prohibited_Cars_Btn() {
        ElementActions.click(driver, List_Prohibited_Cars_Btn);
    }

    public By List_Prohibited_Cars_New_Plate_Number_Btn = By.id("ivps-tool-bar-3d9ee5f4c45bbb230");

    public void Select_List_Prohibited_Cars_New_Plate_Number_Btn() {
        ElementActions.click(driver, List_Prohibited_Cars_New_Plate_Number_Btn);
    }

    public By List_Prohibited_Cars_New_Plate_ID_1 = By.id("cpn1");// Text Area

    public void Type_List_Prohibited_Cars_New_Plate_ID_1() {
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_1, "أ");
    }

    public By List_Prohibited_Cars_New_Plate_ID_2 = By.id("cpn2");// Text Area

    public void Type_List_Prohibited_Cars_New_Plate_ID_2() {
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_2, "ب");
    }

    public By List_Prohibited_Cars_New_Plate_ID_3 = By.id("cpn3");// Text Area

    public void Type_List_Prohibited_Cars_New_Plate_ID_3() {
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_3, "ت");
    }

    public By List_Prohibited_Cars_New_Plate_ID_4 = By.id("cpn4");// Text Area

    public void Type_List_Prohibited_Cars_New_Plate_4() {
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_4, "ث");
    }

    public By List_Prohibited_Cars_New_Plate_ID_5 = By.id("cpn5");// Text Area

    public void Type_List_Prohibited_Cars_New_Plate_5() {
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_5, "1234");
    }

    public void Type_List_Prohibited_Cars_Add_New_Plate_ID() {
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_1, "أ");
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_2, "ب");
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_3, "ت");
        ElementActions.type(driver, List_Prohibited_Cars_New_Plate_ID_5, "1234");
    }

    public By List_Prohibited_Cars_Add_Plate_ID_Btn = By.id("add-license-plate-dialog-550406667bdf6d91");

    public void Select_List_Prohibited_Cars_Add_Plate_ID_Btn() {
        ElementActions.click(driver, List_Prohibited_Cars_Add_Plate_ID_Btn);
    }

    public By List_Prohibited_Cars_Cancel_Plate_ID_Btn = By.id("add-license-plate-dialog-78f74f6940cd11a8");

    public void Select_List_Prohibited_Cars_Cancel_Plate_ID_Btn() {
        ElementActions.click(driver, List_Prohibited_Cars_Cancel_Plate_ID_Btn);
    }

    public By List_Prohibited_Cars_DisplayAll_Plate_ID_Btn = By.id("ivps-tool-bar-3d9ee5f4c45bbb231");

    public void Select_List_Prohibited_Cars_DisplayAll_Plate_ID_Btn() {
        ElementActions.click(driver, List_Prohibited_Cars_DisplayAll_Plate_ID_Btn);
    }

    public By List_Prohibited_Cars_Search_Insert_Plate_ID = By.id("plateNumber"); // Text Area

    public void Type_List_Prohibited_Cars_Search_Insert_Plate_ID() {
        ElementActions.type(driver, List_Prohibited_Cars_Search_Insert_Plate_ID, "أ ب ت ث 1234");
    }

    public By List_Prohibited_Cars_Search_Plate_ID_Btn = By.className("btn btn-success w-100 align-self-end");

    public void Select_List_Prohibited_Cars_Search_Plate_ID_Btn() {
        ElementActions.click(driver, List_Prohibited_Cars_Search_Plate_ID_Btn);
    }

    public By List_Prohibited_Cars_Search_Plate_Count = By.id("whitelist-manager-6ab59e9fbfadaebb");

    public void Get_List_Prohibited_Cars_Search_Plate_Count() {
        ElementActions.getElementsCount(driver, List_Prohibited_Cars_Search_Plate_Count);
    }

    public H8BlackListCarsPage(WebDriver driver) {
        this.driver = driver;
    }
}
