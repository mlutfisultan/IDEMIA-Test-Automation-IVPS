package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.shaft.gui.element.ElementActions.click;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;

public class B2WelcomePage {
    WebDriver driver;

    public By Welcome_Page_Lable = By.cssSelector("#breadcrumb-main-menu > span");

    public By Violation_Management_Btn = By.id("pages-entry-violations-management");

    public void Select_Violation_Management_Btn() {
        ElementActions.click(driver, Violation_Management_Btn);
    }

    public By White_List_Cars_Btn = By.id("pages-entry-whitelist-manager");

    public void Select_White_List_Cars_Btn() {
        ElementActions.click(driver, White_List_Cars_Btn);
    }

    public By Black_List_Cars_Btn = By.id("pages-entry-blacklist-manager");

    public void Select_Black_List_Cars_Btn() {
        ElementActions.click(driver, Black_List_Cars_Btn);
    }

    public By Radars_Btn = By.id("pages-entry-mesta-manager");

    public void Select_Radars_Btn() {
        ElementActions.click(driver, Radars_Btn);
    }

    public By System_User_Management_Btn = By.id("pages-entry-users-management");

    public void Select_System_User_Management_Btn() {
        ElementActions.click(driver, System_User_Management_Btn);
    }

    public By Reports_followUp_Btn = By.id("pages-entry-reports-dashboard");

    public void Select_Reports_followUp_Btn() {
        ElementActions.click(driver, Reports_followUp_Btn);
    }

    public By Monitor_Panel = By.id("header-watchboard-link");

    public void Select_Monitor_Panel() {
        click(driver, Monitor_Panel);
    }

    public By Statistics_Btn = By.id("pages-entry-analytics");

    public void Select_Statistics_Btn() {
        ElementActions.click(driver, Statistics_Btn);
    }

    public By System_Settings_Btn = By.id("header-appconfig-link");

    public void Select_System_Settings_Btn() {
        click(driver, System_Settings_Btn);
    }

    public By Account_Labol = By.id("header-f6fca042553a615f");

    public void Select_Account_Labol() {
        ElementActions.isElementDisplayed(driver, Account_Labol);
    }

    public By Logout_Menu = cssSelector(".p-splitbutton-menubutton > .p-button-icon");// to be change with next beta

    public void Select_Logout_Menu() {
        click(driver, Logout_Menu);
    }

    public By Change_Password_Btn = className("ng-tns-c50-0 p-menuitem ng-star-inserted"); // to be change with next beta

    public void Select_Change_Password_Btn() {
        click(driver, Change_Password_Btn);
    }

    public By Logout_Btn = cssSelector(".ng-tns-c50-0:nth-child(3) > .p-menuitem-link > .p-menuitem-text");// to be change with next beta

    public void Select_Logout_Btn() {
        click(driver, Logout_Btn);
    }

    public void IVPS_Logout() {
        click(driver, Logout_Menu);
        click(driver, Logout_Btn);
    }

    public B2WelcomePage(WebDriver driver) {
        this.driver = driver;
    }
}
