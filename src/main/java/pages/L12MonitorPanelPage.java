package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.shaft.gui.element.ElementActions.click;

public class L12MonitorPanelPage {
    WebDriver driver;

    public By Monitor_Panel = By.id("header-watchboard-link");

    public void Select_Monitor_Panel() {
        click(driver, Monitor_Panel);
    }

    public L12MonitorPanelPage(WebDriver driver) {
        this.driver = driver;
    }
}
