package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class K11ReportsandFollowUpPage {
    WebDriver driver;
    public By Reports_followUp_Btn = By.id("main-menu-f87eb223e99e4a445");

    public void Select_Reports_followUp_Btn() {
        ElementActions.click(driver, Reports_followUp_Btn);
    }

    // Violations Sent to Prosecution Page
    public By Violations_Sent_to_Prosecution_Btn = By.id("main-menu-f87eb223e99e4a441");

    public void Select_Violations_Sent_to_Prosecution_Btn() {
        ElementActions.click(driver, Violations_Sent_to_Prosecution_Btn);
    }

    public By Violations_Sent_to_Prosecution_Date_From = By.cssSelector(".ng-tns-c84-2 > .p-inputtext");

    public void Select_Violations_Sent_to_Prosecution_Date_From() {
        ElementActions.type(driver, Violations_Sent_to_Prosecution_Date_From, "01/01/2020");
    }

    public By Violations_Sent_to_Prosecution_Date_To = By.cssSelector(".ng-tns-c84-3 > .p-inputtext");

    public void Select_Violations_Sent_to_Prosecution_Date_To() {
        ElementActions.type(driver, Violations_Sent_to_Prosecution_Date_To, "28/02/2022");
    }

    public By Violations_Sent_to_Prosecution_Search_Btn = By.cssSelector(".buttons-bottom-padding:nth-child(3) > .btn");

    public void Select_Violations_Sent_to_Prosecution_Search_Btn() {
        ElementActions.click(driver, Violations_Sent_to_Prosecution_Search_Btn);
    }

    public K11ReportsandFollowUpPage(WebDriver driver) {
        this.driver = driver;
    }
}
