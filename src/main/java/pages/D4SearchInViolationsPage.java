package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class D4SearchInViolationsPage {
    WebDriver driver;

    public By Violation_Management_Btn = By.id("main-menu-f87eb223e99e4a440");

    public void Select_Violation_Management_Btn() {
        ElementActions.click(driver, Violation_Management_Btn);
    }

    public By Reconfigure_125_Plate_Number = By.id("plateNumber");
    public By Reconfigure_125_Violation_Code = By.id("violationCode");

    public By Reconfigure_125_model_Btn = By.id("main-menu-f87eb223e99e4a441"); // Dublicate ID

    public void Select_Reconfigure_125_model_Btn() {
        ElementActions.click(driver, Reconfigure_125_model_Btn);
    }

    public By Reconfigure_125_Search_Btn = By.id("dynamic-form-submit-form-button");

    public void Select_Reconfigure_125_Search_Btn() {
        ElementActions.click(driver, Reconfigure_125_Search_Btn);
    }

    public D4SearchInViolationsPage(WebDriver driver) {
        this.driver = driver;
    }
}
