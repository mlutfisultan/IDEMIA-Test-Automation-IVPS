package com.example.idemia_test_automation_ivps;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.A1MainPage;
import pages.B2WelcomePage;
import pages.MainTestPage;

import static org.testng.Assert.assertEquals;

public class A1OldPlateIDE2ETest {
    private WebDriver driver;
    private A1MainPage MainPage_Obj;
    private B2WelcomePage WelcomePage_Obj;

    @BeforeMethod
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1MainPage(driver);
        WelcomePage_Obj = new B2WelcomePage(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        Assert.assertEquals(driver.findElement(WelcomePage_Obj.Welcome_Page_Lable).getAttribute("value"), "القائمة الرئيسية");
    }
}
