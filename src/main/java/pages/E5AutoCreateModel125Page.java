package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class E5AutoCreateModel125Page {
    WebDriver driver;

    public By Violation_Management_Btn = By.id("main-menu-f87eb223e99e4a440");

    public void Select_Violation_Management_Btn() {
        ElementActions.click(driver, Violation_Management_Btn);
    }

    public By Auto_Reconfigure_125_Model_Btn = By.id("main-menu-f87eb223e99e4a442"); // Dublicate ID

    public void Select_Auto_Reconfigure_125_Model_Btn() {
        ElementActions.click(driver, Auto_Reconfigure_125_Model_Btn);
    }

    public By Auto_Reconfigure_125_Model_Create_Forms_Automatically_Btn = By.id("ivps-tool-bar-3d9ee5f4c45bbb230");

    public void Select_Auto_Reconfigure_125_Model_Create_Forms_Automatically_Btn() {
        ElementActions.click(driver, Auto_Reconfigure_125_Model_Create_Forms_Automatically_Btn);
    }

    public By Auto_Reconfigure_125_Model_Current_Forms_Btn = By.id("ivps-tool-bar-3d9ee5f4c45bbb231");

    public void Select_Auto_Reconfigure_125_Model_Current_Forms_Btn() {
        ElementActions.click(driver, Auto_Reconfigure_125_Model_Current_Forms_Btn);
    }

    public By Auto_Reconfigure_125_Model_Old_Forms_Btn = By.id("ivps-tool-bar-3d9ee5f4c45bbb232");

    public void Select_Auto_Reconfigure_125_Model_Old_Forms_Btn() {
        ElementActions.click(driver, Auto_Reconfigure_125_Model_Old_Forms_Btn);
    }

    public By Auto_Reconfigure_125_Model_Total_Forms_Count = By.id("6a24dcd425ad272b");

    public void Select_Auto_Reconfigure_125_Model_Total_Forms_Count() {
        ElementActions.getElementsCount(driver, Auto_Reconfigure_125_Model_Total_Forms_Count);
    }
    public E5AutoCreateModel125Page(WebDriver driver) {
        this.driver = driver;
    }
}
